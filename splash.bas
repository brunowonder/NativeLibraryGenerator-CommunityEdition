﻿B4J=true
Group=Default Group
ModulesStructureVersion=1
Type=Class
Version=5.9
@EndOfDesignText@
'Class module
Sub Class_Globals
    Private fx As JFX
    Private lblTitle     As Label
    Private lblExit      As Label
    Private lblSound     As Label
    Private LogoClick    As Boolean
    Private cvs          As Canvas
    Private cv2          As Canvas
    Public  splashScreen As Form

    Dim ninjaLogo As Image
    Dim ninjaLogoPosX, ninjaLogoPosY As Double

    Dim skin As Image
    Dim filter As Image

    Dim spinnerLogo As Image
    Dim spinnerPosX, spinnerPosY As Double
    Dim spinnerAngle As Double
    Dim spinnerSpeed As Double

    Dim aspinnerLogo As Image
    Dim aspinnerPosX, aspinnerPosY As Double
    Dim aspinnerAngle As Double
    Dim aspinnerSpeed As Double

    Dim clickTS As Long
    Dim offset As Double

End Sub

'Initializes the object. You can add parameters to this method if needed.
Public Sub Initialize
    splashScreen.Initialize("splashScreen", 1024, 600)
    splashScreen.Title = "[C/C++] Native Library Generator - v" & Main.curVer
    splashScreen.SetFormStyle("TRANSPARENT")
    splashScreen.Resizable = False
    splashScreen.Show
    splashScreen.Stylesheets.Add(File.GetUri(File.DirAssets, "style.css"))
    splashScreen.RootPane.Style = "-fx-background-color: rgba(48, 48, 48, 1); -fx-border-color: rgba(64, 64, 64, 1);"

    skin.Initialize(File.DirAssets, "splash_skin.png")
    filter.Initialize(File.DirAssets, "splash_filter.png")
    ninjaLogo.Initialize(File.DirAssets, "splash.png")
    spinnerLogo.Initialize(File.DirAssets, "spinner.png")
    aspinnerLogo.Initialize(File.DirAssets, "metalgear.png")

    cvs.Initialize("cvs")
    cv2.Initialize("cv2")
    cv2.Alpha = 0.4
End Sub

Public Sub Show
    splashScreen.show

    lblTitle.Initialize("lblTitle")
    lblTitle.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1);"
    lblTitle.Text = " Native Library Generator - v" & Main.curVer


    lblExit.Initialize("lblExit")
    lblExit.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1);"
    lblExit.Text = "X "


    lblSound.Initialize("lblSound")
    lblSound.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1);"
    If Main.sound = False Then
        lblSound.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 96, 160, 1);"
    End If
    lblSound.Text = "Snd"

    splashScreen.RootPane.AddNode(cvs, 0, 0, splashScreen.Width, splashScreen.Height)
    splashScreen.RootPane.AddNode(cv2, 0, 0, splashScreen.Width, splashScreen.Height)
    splashScreen.RootPane.AddNode(lblTitle, 1, 1, splashScreen.Width - 2, splashScreen.Height * 0.05)
    splashScreen.RootPane.AddNode(lblSound, splashScreen.Width - 50, 1, 19, splashScreen.Height * 0.05)
    splashScreen.RootPane.AddNode(lblExit, splashScreen.Width - 20, 1, 19, splashScreen.Height * 0.05)

    spinnerPosX = (splashScreen.Width * 0.67) - (58 / 2)
    spinnerPosY = (splashScreen.Height * 0.45) - (58 / 2)
    spinnerAngle = 0
    spinnerSpeed = -0.1
    'spinnerSpeedDir = -1

    aspinnerPosX = (splashScreen.Width * 0.67) - (125 / 2)
    aspinnerPosY = (splashScreen.Height * 0.45) - (125 / 2)
    aspinnerAngle = 0
    aspinnerSpeed = 0.1
    'aspinnerSpeedDir = 1

    ninjaLogoPosX = (splashScreen.Width * 0.5) - (519 / 2)
    ninjaLogoPosY = (splashScreen.Height * 0.5) - (250 / 2)
    offset = ninjaLogoPosY
    'aspinnerSpeedDir = -1

End Sub

Public Sub Close
    splashScreen.Close
End Sub

Public Sub Opening(timeStamp As Long) As Boolean
    splashScreen.WindowLeft = Main.wL
    splashScreen.WindowTop  = Main.wT
    cv2.Alpha = 0.3 + Abs(SinD(DateTime.Now * 10)) * 0.2
    cvs.ClearRect(0, 0, splashScreen.Width, splashScreen.Height)
    cv2.ClearRect(0, 0, splashScreen.Width, splashScreen.Height)

    If LogoClick Then
        spinnerSpeed = spinnerSpeed * 1.02
        aspinnerSpeed = aspinnerSpeed * 1.02
        spinnerAngle = spinnerAngle + spinnerSpeed
        aspinnerAngle = aspinnerAngle + aspinnerSpeed
    End If

    If LogoClick And DateTime.Now - clickTS > 2000 Then
        splashScreen.WindowHeight = Max(0, splashScreen.WindowHeight - 20)
        spinnerPosY = spinnerPosY - 20
        aspinnerPosY = aspinnerPosY - 20
        ninjaLogoPosY = ninjaLogoPosY - 20
        If splashScreen.WindowHeight = 0 Then
            spinnerPosY = -290 + ((splashScreen.Height * 0.45) - (58 / 2))
            aspinnerPosY = -290 + ((splashScreen.Height * 0.45) - (125 / 2))
            ninjaLogoPosY = -260 + ((splashScreen.Height * 0.5) - (250 / 2))
            LogoClick = False
            Close
            Return True
        End If
    End If

    cvs.DrawImage(skin, 0, ninjaLogoPosY - offset, splashScreen.Width, 600)
    cvs.DrawImageRotated(aspinnerLogo, aspinnerPosX, aspinnerPosY, 125, 125, aspinnerAngle)
    cvs.DrawImageRotated(spinnerLogo, spinnerPosX, spinnerPosY, 58, 58, spinnerAngle)
    cvs.DrawImage(ninjaLogo, ninjaLogoPosX, ninjaLogoPosY, 519, 250)
    cv2.DrawImage(filter, 0, ninjaLogoPosY - offset, splashScreen.Width, 600)
    Return False
End Sub

Public Sub Closing As Boolean
    splashScreen.WindowLeft = Main.wL
    splashScreen.WindowTop  = Main.wT
    cv2.Alpha = 0.3 + Abs(SinD(DateTime.Now * 10)) * 0.2
    cvs.ClearRect(0, 0, splashScreen.Width, splashScreen.Height)
    cv2.ClearRect(0, 0, splashScreen.Width, splashScreen.Height)

    If Not(splashScreen.Showing) Then splashScreen.show
    splashScreen.WindowHeight = Min(600, splashScreen.WindowHeight + 10)

    spinnerPosY = Min(spinnerPosY + 10, ((splashScreen.Height * 0.45) - (58 / 2)))
    aspinnerPosY = Min(aspinnerPosY + 10, ((splashScreen.Height * 0.45) - (125 / 2)))
    ninjaLogoPosY = Min(ninjaLogoPosY + 10, ((splashScreen.Height * 0.5) - (250 / 2)))


    spinnerSpeed = spinnerSpeed * (0.98)
    spinnerAngle = spinnerAngle + spinnerSpeed

    aspinnerSpeed = aspinnerSpeed * (0.98)
    aspinnerAngle = aspinnerAngle + aspinnerSpeed

    If aspinnerSpeed <= 0.02 Then aspinnerSpeed = 0
    If spinnerSpeed >= -0.02 Then spinnerSpeed = 0

    cvs.DrawImage(skin, 0, ninjaLogoPosY - offset, splashScreen.Width, 600)
    cvs.DrawImageRotated(aspinnerLogo, aspinnerPosX, aspinnerPosY, 125, 125, aspinnerAngle)
    cvs.DrawImageRotated(spinnerLogo, spinnerPosX, spinnerPosY, 58, 58, spinnerAngle)
    cvs.DrawImage(ninjaLogo, ninjaLogoPosX, ninjaLogoPosY, 519, 250)
    cv2.DrawImage(filter, 0, ninjaLogoPosY - offset, splashScreen.Width, 600)

    If (splashScreen.WindowHeight = 600 And aspinnerSpeed = 0 And spinnerSpeed = 0) Then
        Return True
    End If

    Return False
End Sub

Public Sub splashScreen_MouseClicked(event As MouseEvent)
    If event.Y > splashScreen.Height * 0.05 Then
        If LogoClick = False Then
            clickTS = DateTime.Now
            LogoClick = True
        Else
            clickTS = 0
        End If
    End If
End Sub

Sub lblSound_MousePressed(mouse As MouseEvent)
    If Main.sound Then
        File.WriteString(File.DirApp, "sound.txt", "0")
        Main.sound = False
        Main.bgMusic.Pause
        lblSound.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 96, 160, 1);"
    Else
        File.WriteString(File.DirApp, "sound.txt", "1")
        Main.sound = True
        Main.bgMusic.Play
        lblSound.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1);"
    End If
End Sub

Sub lblExit_MousePressed(mouse As MouseEvent)
    lblExit.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(255, 32, 32, 1); -fx-background-color: rgba(40, 40, 40, 1);"
End Sub

Sub lblExit_MouseDragged(mouse As MouseEvent)
    If Not(mouse.X >= 0 And mouse.X <= lblExit.Width And mouse.Y >= 0 And mouse.Y <= lblExit.Height) Then
        lblExit.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1); -fx-background-color: rgba(40, 40, 40, 1);"
    Else
        lblExit.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(255, 32, 32, 1); -fx-background-color: rgba(40, 40, 40, 1);"
    End If
End Sub

Sub lblExit_MouseReleased(mouse As MouseEvent)
    lblExit.Style = "-fx-vertical-align: text-top; -fx-font: 12px Consolas; -fx-text-fill: rgba(0, 192, 255, 1); -fx-background-color: rgba(40, 40, 40, 1);"
    ExitApplication
End Sub

Sub setSoundLabel(css As String)
    lblSound.Style = css
End Sub