Native Library Generator - Community Edition
Copyright (C) 2015-2017  Bruno Silva / Ninja Dynamics
Homepage: https://goo.gl/4eXViK

This program allows you to write or import C/C++ code and have all the
functions compiled into a native (.so) library, which in turn is included in
a ready-to-use B4A Java lib. When using already existing code, if present, the
main() function will be automatically ignored. If you're familiar with C/C++
but not with JNI or NDK, this is the right tool for you! No JAVA required.

As happy as I am to share this work with the B4x community, let's not forget
that this piece of software was initially created as an internal tool, a never
ending work-in-progress personal project. The source code if far from perfect.

Enjoy! :)
