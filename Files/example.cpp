/*
    =============================================================
    MazeSolver - DFS (Iterative/Recursive) Path Finding Algorithm
    =============================================================
    Written by Bruno Silva (Ninja Dynamics)
    Based on Frédérique Defoort's SDA algorithm: 
    http://www.b4x.com/android/forum/threads/optimization-with-b4a.57913

    Last Revision: 2016-07-11
	
    This library is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library.  If not, see <http://www.gnu.org/licenses/>.	
*/

#include <cstdlib>
#include <cmath>
#include <stdbool.h>
#include <ctime>
#include <vector>
#include <stack>
#include <algorithm>

//Constants
#define PI            3.141592653589793238462643383279502884197169399375105
#define TORADIANS     0.017453292519943295769236907684886127134428718885417
#define TODEGREES     57.29577951308232087679815481410517033240547246656432
#define MAGICNUMBER   0x5f3759df

//Structs
typedef struct {	
	int **value;
	int sizeX, sizeY;
	int bmpStartX, bmpStartY;
	int bmpDestX, bmpDestY;
	int startX, startY;
	int destX, destY;
	int destIndex;
	int stackFrames;
	int stackLimit;	
	double invSizeX;
	double pathLenght;
	std::vector<int> waypointX;
	std::vector<int> waypointY;
	std::stack<int> currentState;
	uintptr_t memoryAddress;
	uintptr_t childAddress;
} MazeMatrix;

//Global Variables
MazeMatrix *currentMatrix = nullptr;

//Randomization
void SetPRNGSeed(long seed);

//Math
double privateCos(double x);
double privateSin(double x);
double CalcAngle(uintptr_t matrixAddress, int wpA, int wpB, bool degrees);

//Native Array Tools
int       NativeArrayGetElement(uintptr_t arrayAddress, int position);
int       NativeArrayGetSize(uintptr_t arrayAddress);
void      NativeArraySetElement(uintptr_t arrayAddress, int position, int value);
void      NativeArrayReset(uintptr_t arrayAddress);
void      NativeArrayDispose(uintptr_t arrayAddress);
uintptr_t NativeArrayCreateNew(int size);

//Maze Matrix Tools
bool      MazeMatrixLoadBMP(uintptr_t matrixAddress, char* filename);
void      MazeMatrixGenerateRandomData(uintptr_t matrixAddress, int densityPercentage);
int       MazeMatrixGetSizeX(uintptr_t matrixAddress);
int       MazeMatrixGetSizeY(uintptr_t matrixAddress);
int       MazeMatrixGetElement(uintptr_t matrixAddress, int x, int y);
void      MazeMatrixSetElement(uintptr_t matrixAddress, int x, int y, int value);
void	  MazeMatrixAddWaypoint(uintptr_t matrixAddress, int x, int y);
void	  MazeMatrixRemoveWaypoint(uintptr_t matrixAddress);
void	  MazeMatrixResetResults(uintptr_t matrixAddress);
int       MazeMatrixGetWaypointX(uintptr_t matrixAddress, int index);
int       MazeMatrixGetWaypointY(uintptr_t matrixAddress, int index);
int       MazeMatrixGetWaypointArraySize(uintptr_t matrixAddress);
void      MazeMatrixReset(uintptr_t matrixAddress);
void      MazeMatrixDispose(uintptr_t matrixAddress);
uintptr_t MazeMatrixCreateNew(int sizeX, int sizeY);
uintptr_t MazeMatrixGetChildArrayAddress(uintptr_t matrixAddress);
bool      privateMazeMatrixProcessWaypoints(uintptr_t matrixAddress, double pathLenght);

//AI
uintptr_t Initialize(int sizeX, int sizeY);
void      Dispose(uintptr_t matrixAddress);
bool      Solve(uintptr_t mazeMatrixAddress, int startX, int startY, int destX, int destY, bool optimizePath, int stackLimit);
bool      SolvePlus(uintptr_t mazeMatrixAddress, int startX, int startY, int destX, int destY, int stackLimit);
bool      privateRecursiveFLMAlgoSDA(int index);
bool      privateIterativeFLMAlgoSDA(int index);

double privateCos(double x)
{
	//Always wrap input angle to -PI..PI	
	if (x > 6.28318531) {
		double n = truncl(x * 0.15915494309);
		x -= (6.28318531 * n);
	}
	else if (x < -6.28318531) {
		double n = truncl(x * 0.15915494309);
		x += (6.28318531 * -n);
	}
	if (x < -3.14159265)
		x += 6.28318531;
	else
		if (x >  3.14159265)
			x -= 6.28318531;

	//Compute cosine: sin(x + PI/2) = cos(x)
	x += 1.57079632;
	if (x >  3.14159265)
		x -= 6.28318531;

	double ans = 0.0;
	if (x < 0) {
		ans = 1.27323954 * x + 0.405284735 * x * x;
		if (ans < 0)
			ans = .225 * (ans *-ans - ans) + ans;
		else
			ans = .225 * (ans * ans - ans) + ans;
	}
	else {
		ans = 1.27323954 * x - 0.405284735 * x * x;
		if (ans < 0)
			ans = .225 * (ans *-ans - ans) + ans;
		else
			ans = .225 * (ans * ans - ans) + ans;
	}
	if (ans <= 0.000001 && ans >= -0.000001)
		ans = 0;
	else if (ans <= -0.999999)
		ans = -1;
	else if (ans >= 0.999999)
		ans = 1;

	return ans;
}

double privateSin(double x)
{
	//Always wrap input angle to -PI..PI	
	if (x > 6.28318531) {
		double n = truncl(x * 0.15915494309);
		x -= (6.28318531 * n);
	}
	else if (x < -6.28318531) {
		double n = truncl(x * 0.15915494309);
		x += (6.28318531 * -n);
	}
	if (x < -3.14159265)
		x += 6.28318531;
	else
		if (x >  3.14159265)
			x -= 6.28318531;

	//Compute Sine
	double ans = 0.0;
	if (x < 0) {
		ans = 1.27323954 * x + .405284735 * x * x;
		if (ans < 0)
			ans = .225 * (ans *-ans - ans) + ans;
		else
			ans = .225 * (ans * ans - ans) + ans;
	}
	else {
		ans = 1.27323954 * x - 0.405284735 * x * x;
		if (ans < 0)
			ans = .225 * (ans *-ans - ans) + ans;
		else
			ans = .225 * (ans * ans - ans) + ans;
	}

	if (ans <= 0.000001 && ans >= -0.000001)
		ans = 0;
	else if (ans <= -0.999999)
		ans = -1;
	else if (ans >= 0.999999)
		ans = 1;

	return ans;
}


/*Creates a new native array
You must DISPOSE this object once you don't need it anymore*/
uintptr_t NativeArrayCreateNew(int size)
{
	int *workingArray = nullptr;
	workingArray = (int*)malloc(sizeof(int) * (size + 1));
	workingArray[0] = size;
	int i;
	for (i = 1; i < size + 1; ++i) {
		workingArray[i] = 0;
	}
	return (uintptr_t)workingArray;
}

//Assigns the given value to the given array index
void NativeArraySetElement(uintptr_t arrayAddress, int position, int value)
{
	int *workingArray = (int*)arrayAddress;
	workingArray[position + 1] = value;
}

//Obtains the value stored at the given index
int NativeArrayGetElement(uintptr_t arrayAddress, int position)
{
	int *workingArray = (int*)arrayAddress;
	return workingArray[position + 1];
}

//Obtains the array size
int NativeArrayGetSize(uintptr_t arrayAddress)
{
	int *workingArray = (int*)arrayAddress;
	return workingArray[0];
}

//Resets the array content
void NativeArrayReset(uintptr_t arrayAddress)
{
	int *workingArray = (int*)arrayAddress;
	int i;
	for (i = 1; i < workingArray[0] + 1; ++i) {
		workingArray[i] = 0;
	}
}

//Disposes the native array
void NativeArrayDispose(uintptr_t arrayAddress)
{
	int *workingArray = (int*)arrayAddress;
	free(workingArray);
	workingArray = nullptr;
}

/*Creates a new maze matrix
You must DISPOSE this object once you don't need it anymore*/
uintptr_t MazeMatrixCreateNew(int sizeX, int sizeY)
{
	MazeMatrix *workingMatrix = new MazeMatrix;
	workingMatrix->sizeX = sizeX;
	workingMatrix->sizeY = sizeY;
	workingMatrix->value = (int**)malloc(sizeof(int*) * sizeX);
	int x, y;
	for (x = 0; x < sizeX; ++x) {
		workingMatrix->value[x] = (int*)malloc(sizeof(int) * sizeY);
		for (y = 0; y < sizeY; ++y) {
			workingMatrix->value[x][y] = 0;
		}
	}
	workingMatrix->bmpStartX = -1;
	workingMatrix->bmpStartY = -1;
	workingMatrix->bmpDestX = -1;
	workingMatrix->bmpDestY = -1;
	workingMatrix->pathLenght = 0;
	workingMatrix->stackFrames = 0;
	workingMatrix->stackLimit = 0;
	workingMatrix->startX = -1;
	workingMatrix->startY = -1;
	workingMatrix->destX = -1;
	workingMatrix->destY = -1;
	workingMatrix->destIndex = -1;
	workingMatrix->invSizeX = 0.0;
	workingMatrix->childAddress = (uintptr_t)nullptr;
	workingMatrix->memoryAddress = (uintptr_t)workingMatrix;
	return workingMatrix->memoryAddress;
}

//Resets the whole matrix content, including path finding results
void MazeMatrixReset(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	int x, y;
	for (x = 0; x < workingMatrix->sizeX; ++x) {
		for (y = 0; y < workingMatrix->sizeY; ++y) {
			workingMatrix->value[x][y] = 0;
		}
	}
	if (workingMatrix->childAddress != (uintptr_t)nullptr) {
		NativeArrayReset(workingMatrix->childAddress);
	}
	workingMatrix->startX = -1;
	workingMatrix->startY = -1;
	workingMatrix->destX = -1;
	workingMatrix->destY = -1;
	workingMatrix->destIndex = -1;
	workingMatrix->invSizeX = 0.0;
	MazeMatrixResetResults(matrixAddress);
}

//Adds a Waypoint using the std::vector push_back() function
void MazeMatrixAddWaypoint(uintptr_t matrixAddress, int x, int y)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	workingMatrix->waypointX.push_back(x);
	workingMatrix->waypointY.push_back(y);
}

//Removes the last Waypoint using the std::vector pop_back() function
void MazeMatrixRemoveWaypoint(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	workingMatrix->waypointX.pop_back();
	workingMatrix->waypointY.pop_back();
}

//Resets all matrix content concerning only the results obtained with Solve() or SolvePlus()
void MazeMatrixResetResults(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	workingMatrix->bmpStartX = -1;
	workingMatrix->bmpStartY = -1;
	workingMatrix->bmpDestX = -1;
	workingMatrix->bmpDestY = -1;
	workingMatrix->pathLenght = 0;
	workingMatrix->stackFrames = 0;
	workingMatrix->waypointX.clear();
	workingMatrix->waypointY.clear();
}

//Obtains the X coordinate of the given Waypoint
int MazeMatrixGetWaypointX(uintptr_t matrixAddress, int index)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->waypointX.at(index);
}

//Obtains the Y coordinate of the given Waypoint
int MazeMatrixGetWaypointY(uintptr_t matrixAddress, int index)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->waypointY.at(index);
}

//Obtains the Waypoint array (std::vector) size
int MazeMatrixGetWaypointArraySize(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->waypointX.size();
}

bool privateMazeMatrixProcessWaypoints(uintptr_t matrixAddress, double pathLenght)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);

	std::vector<int> workingWaypointX = workingMatrix->waypointX;
	std::vector<int> workingWaypointY = workingMatrix->waypointY;
	workingMatrix->pathLenght = 0.0;
	workingMatrix->waypointX.clear();
	workingMatrix->waypointY.clear();
	workingMatrix->waypointX.push_back(workingWaypointX.at(workingWaypointX.size() - 1));
	workingMatrix->waypointY.push_back(workingWaypointY.at(workingWaypointY.size() - 1));

	int destination = workingWaypointX.size() - 1;
	int startingPoint = 0;

	while (destination > 0)
	{
		int destX = workingWaypointX.at(destination);
		int destY = workingWaypointY.at(destination);
		int oX = destX;
		int oY = destY;

		int iteration = -1;
		while (startingPoint < destination)
		{
			int startX = workingWaypointX.at(startingPoint);
			int startY = workingWaypointY.at(startingPoint);
			double deltaX = startX - oX;
			double deltaY = startY - oY;

			double angle = atan2(deltaY, deltaX);
			double cosAngle = privateCos(angle);
			double sinAngle = privateSin(angle);

			//double cosAngle = (int)(cos(angle) * 1000000000) * 0.000000001;
			//double sinAngle = (int)(sin(angle) * 1000000000) * 0.000000001;

			iteration++;
			destX = (oX + (int)(cosAngle * iteration));
			destY = (oY + (int)(sinAngle * iteration));

			if (!(destX == startX && destY == startY)) {
				if (cosAngle != 0 && sinAngle != 0) {
					if ((cosAngle < 0 && destX - 1 >= 0 && workingMatrix->value[destX - 1][destY] != 0) ||
						(cosAngle > 0 && destX + 1 < workingMatrix->sizeX && workingMatrix->value[destX + 1][destY] != 0) ||
						(sinAngle < 0 && destY - 1 >= 0 && workingMatrix->value[destX][destY - 1] != 0) ||
						(sinAngle > 0 && destY + 1 < workingMatrix->sizeY && workingMatrix->value[destX][destY + 1] != 0))
					{
						++startingPoint;
						iteration = -1;
						continue;
					}
				}
			}

			if (iteration == 0) continue;

			if (workingMatrix->value[destX][destY] != 0) {
				++startingPoint;
				iteration = 0;
				continue;
			}
			else if (destX == startX && destY == startY) {
				workingMatrix->pathLenght += sqrt((deltaX * deltaX) + (deltaY * deltaY));
				if (pathLenght >= 0.0 && workingMatrix->pathLenght >= pathLenght) return false;
				workingMatrix->waypointX.push_back(startX);
				workingMatrix->waypointY.push_back(startY);
				destination = startingPoint;
				break;
			}
		}
		startingPoint = 0;
	}
	if (pathLenght < 0.0) {
		std::reverse(workingMatrix->waypointX.begin(), workingMatrix->waypointX.end());
		std::reverse(workingMatrix->waypointY.begin(), workingMatrix->waypointY.end());
	}
	//for (int i = 0; i < MazeMatrixGetWaypoNativeArraySize(matrixAddress); i++)
	//	printf("%d, %d: %.3f\n", MazeMatrixGetWaypointX(matrixAddress, i), MazeMatrixGetWaypointY(matrixAddress, i), workingMatrix->pathLenght);	
	//system("pause");
	return true;
}

/*Assigns the given value to the given matrix (x,y) index
Use this function to build your "maze world"
Use 0 for "free space" and 1 for "blocks/walls"*/
void MazeMatrixSetElement(uintptr_t matrixAddress, int x, int y, int value)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	workingMatrix->value[x][y] = value;
}

//Obtains the value stored at the given matrix (x,y) index
int MazeMatrixGetElement(uintptr_t matrixAddress, int x, int y)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->value[x][y];
}

//Obtains the matrix X (horizontal) size, meaning the number of columns
int MazeMatrixGetSizeX(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->sizeX;
}

//Obtains the matrix Y (vertical) size, meaning the number of rows
int MazeMatrixGetSizeY(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->sizeY;
}

//Obtains the matrix's child array memory address
uintptr_t MazeMatrixGetChildArrayAddress(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	return workingMatrix->childAddress;
}

//Calculates the vector angle of [WaypointA-->WaypointB]
double CalcAngle(uintptr_t matrixAddress, int wpA, int wpB, bool degrees)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	if (workingMatrix->waypointX.empty() || workingMatrix->waypointY.empty() || wpA < 0 || wpB < 0 ||
		(unsigned int)wpA >= workingMatrix->waypointX.size() || (unsigned int)wpA >= workingMatrix->waypointY.size() ||
		(unsigned int)wpB >= workingMatrix->waypointX.size() || (unsigned int)wpB >= workingMatrix->waypointY.size()) {
		return NULL;
	}
	double deltaX = workingMatrix->waypointX.at(wpB) - workingMatrix->waypointX.at(wpA);
	double deltaY = workingMatrix->waypointY.at(wpB) - workingMatrix->waypointY.at(wpA);
	if (degrees) {
		return atan2(deltaY, deltaX) * TODEGREES;
	}
	return atan2(deltaY, deltaX);
}

/*Sets the Pseudo-Random Number Generator seed
This function should be called only one time and prior to MazeMatrixGenerateRandomData()
It is recommended to call it from either Activity_Create or Activity_Resume
Example: <code>SetPRNGSeed(DateTime.Now)</code>*/
void SetPRNGSeed(long seed)
{
	srand((unsigned int)seed);
}

//Generates random maze data
void MazeMatrixGenerateRandomData(uintptr_t matrixAddress, int densityPercentage)
{	
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	MazeMatrixReset(matrixAddress);
	
	if (densityPercentage < 0) {
		densityPercentage = rand() % 101;
	}
	for (int y = 0; y < workingMatrix->sizeY; y++) {
		for (int x = 0; x < workingMatrix->sizeX; x++)
		{
			int value = rand() % 101;
			if (value <= densityPercentage) {
				value = 1;
			}
			else {
				value = 0;
			}
			workingMatrix->value[x][y] = value;
		}
	}
}

/*Loads maze data from a given bitmap file
Only white pixels RGB(255, 255, 255) count as free space. All other colors (RGB values) will count as blocks/walls.
The file may NOT be located at File.DirAsstes, see workaround code below:
<code>File.Copy(File.DirAssets, "maze.bmp", File.DirDefaultExternal, "maze.bmp")
MazeMatrixLoadBMP(myMaze, File.DirDefaultExternal & "/maze.bmp")
File.Delete(File.DirDefaultExternal, "maze.bmp")</code>*/
bool MazeMatrixLoadBMP(uintptr_t matrixAddress, char* filename)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	MazeMatrixReset(matrixAddress);

	FILE* f = fopen(filename, "rb");
	if (f == NULL)
		return false;

	//Reads the 54-byte header
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, f);

	//Extracts the image width and height from the header
	int width = *(int*)&info[18];
	int height = *(int*)&info[22];

	int row_padded = (width * 3 + 3) & (~3);
	unsigned char* data = new unsigned char[row_padded];
	unsigned char tmp;

	for (int i = 0; i < height; i++)
	{
		fread(data, sizeof(unsigned char), row_padded, f);
		for (int j = 0; j < width * 3; j += 3)
		{
			// Convert (B, G, R) to (R, G, B)
			tmp = data[j];
			data[j] = data[j + 2];
			data[j + 2] = tmp;

			int r = data[j];
			int g = data[j + 1];
			int b = data[j + 2];

			if (j % 3 == 0 && j / 3 < workingMatrix->sizeX && i < workingMatrix->sizeY) {
				int x = j / 3;
				int y = (workingMatrix->sizeY - 1) - i;
				if (r == 255 && g == 255 && b == 255) {
					workingMatrix->value[x][y] = 0;
				}
				else if (r < 64 && g > 192 && b < 64) {
					workingMatrix->value[x][y] = 0;
					workingMatrix->bmpStartX = x;
					workingMatrix->bmpStartY = y;
				}
				else if (r > 192 && g < 64 && b < 64) {
					workingMatrix->value[x][y] = 0;
					workingMatrix->bmpDestX = x;
					workingMatrix->bmpDestY = y;
				}
				else {
					workingMatrix->value[x][y] = 1;
				}
			}
		}
	}
	delete data;
	fclose(f);
	return true;
}

//Disposes the maze matrix object
void MazeMatrixDispose(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);	
	for (int x = 0; x < workingMatrix->sizeX; ++x) {
		free(workingMatrix->value[x]);
	}
	free(workingMatrix->value);
	delete workingMatrix;
}

//Initializes the Maze Solver
uintptr_t Initialize(int sizeX, int sizeY)
{
	uintptr_t matrixAddress = MazeMatrixCreateNew(sizeX, sizeY);
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	workingMatrix->childAddress = NativeArrayCreateNew(sizeX * sizeY);
	return matrixAddress;
}

//Disposes the Maze Solver
void Dispose(uintptr_t matrixAddress)
{
	MazeMatrix *workingMatrix = (MazeMatrix*)(matrixAddress);
	NativeArrayDispose(workingMatrix->childAddress);
	MazeMatrixDispose(matrixAddress);
}

/*Solves the maze (one-pass, A->B) and saves the solution in two (x,y) waypoint arrays
Setting the stackLimit to -1 will use iteration instead of recursion*/
bool Solve(uintptr_t mazeMatrixAddress, int startX, int startY, int destX, int destY, bool optimizePath, int stackLimit)
{
	currentMatrix = (MazeMatrix*)(mazeMatrixAddress);

	//Overrides the function arguments in case of BMPLoad with defined start/destination points
	if (currentMatrix->bmpStartX != -1 && currentMatrix->bmpStartY != -1 && currentMatrix->bmpDestX != -1 && currentMatrix->bmpDestY != -1) {
		startX = currentMatrix->bmpStartX;
		startY = currentMatrix->bmpStartY;
		destX = currentMatrix->bmpDestX;
		destY = currentMatrix->bmpDestY;
	}

	//Prepares to solve [A -> B]
	uintptr_t visitedArrayAddress = MazeMatrixGetChildArrayAddress(mazeMatrixAddress);
	NativeArrayReset(visitedArrayAddress);
	MazeMatrixResetResults(mazeMatrixAddress); //BMP start/end points are destroyed here (= -1)
	if (MazeMatrixGetElement(mazeMatrixAddress, startX, startY) == 1 || MazeMatrixGetElement(mazeMatrixAddress, destX, destY) == 1) {
		return false;
	}

	currentMatrix->startX = startX;
	currentMatrix->startY = startY;
	currentMatrix->destX = destX;
	currentMatrix->destY = destY;
	currentMatrix->destIndex = destX + (destY * (currentMatrix->sizeX));
	currentMatrix->invSizeX = 1.0 / (currentMatrix->sizeX);
	currentMatrix->stackLimit = stackLimit;
	int index = startX + (startY * (currentMatrix->sizeX));

	//Solves [A -> B] and in case there is a solution:
	if (stackLimit < 0) {
		if (privateIterativeFLMAlgoSDA(index)) {
			if (optimizePath) {
				privateMazeMatrixProcessWaypoints(mazeMatrixAddress, -1);
			}
			return true;
		}
	}
	else {
		if (privateRecursiveFLMAlgoSDA(index)) {
			if (optimizePath) {
				privateMazeMatrixProcessWaypoints(mazeMatrixAddress, -1);
			}
			return true;
		}
	}
	return false;
}


/*Solves the maze (two-pass, A->B and B->A) and saves the best solution (A->B) in two (x,y) waypoint arrays
Setting the stackLimit to -1 will use iteration instead of recursion*/
bool SolvePlus(uintptr_t mazeMatrixAddress, int startX, int startY, int destX, int destY, int stackLimit)
{
	//Prepares to find the best solution
	MazeMatrix *workingMatrix = (MazeMatrix*)(mazeMatrixAddress);

	//Solves [A -> B] and in case there is a solution:
	if (Solve(mazeMatrixAddress, startX, startY, destX, destY, false, stackLimit)) {

		//Processes [A -> B] waypoints
		privateMazeMatrixProcessWaypoints(mazeMatrixAddress, -1);

		//Backups post-processed [A -> B] info
		double pathLenghtAB = workingMatrix->pathLenght;
		std::vector<int> AtoBwaypointX = workingMatrix->waypointX;
		std::vector<int> AtoBwaypointY = workingMatrix->waypointY;

		//Solves [B -> A]
		Solve(mazeMatrixAddress, workingMatrix->destX, workingMatrix->destY, workingMatrix->startX, workingMatrix->startY, false, stackLimit);

		//Processes [B -> A] waypoints and chooses the most optimal path		
		if (privateMazeMatrixProcessWaypoints(mazeMatrixAddress, pathLenghtAB)) {
			//The current alternative path [B -> A] is the right answer			
		}
		else {
			//Sets the orignal path [A -> B] as the right answer
			workingMatrix->waypointX = AtoBwaypointX;
			workingMatrix->waypointY = AtoBwaypointY;
		}
		int swapX = workingMatrix->startX;
		int swapY = workingMatrix->startY;
		workingMatrix->startX = workingMatrix->destX;
		workingMatrix->startY = workingMatrix->destY;
		workingMatrix->destX = swapX;
		workingMatrix->destY = swapY;
		return true;
	}
	return false;
}

bool privateIterativeFLMAlgoSDA(int index)
{
	//Initializes the result and sets the first stack frame
	bool result = false;
	currentMatrix->currentState.push(index);

	while (!(currentMatrix->currentState.empty()))
	{
		//Gets the index from the top of the Stack
		index = currentMatrix->currentState.top();

		//Marks as visited	
		int *workingArray = (int*)currentMatrix->childAddress;
		workingArray[index + 1] = 1;

		//Computes the horizontal and vertical distances
		int y = (int)(index * currentMatrix->invSizeX);
		int x = index - (y * currentMatrix->sizeX);
		int distX = (currentMatrix->destX) - x;
		int distY = (currentMatrix->destY) - y;

		//If the goal is reached...
		if (index == currentMatrix->destIndex) {
			currentMatrix->waypointX.push_back(x);
			currentMatrix->waypointY.push_back(y);
			result = true;
			while (!(currentMatrix->currentState.empty())) {
				currentMatrix->currentState.pop();
			}
			break;
		}

		//Selects the axis to follow
		bool horizontal = false;
		if (distX > 0) {
			if (distY > 0) {
				horizontal = (distX > distY);
			}
			else {
				horizontal = (distX > -distY);
			}
		}
		else {
			if (distY > 0) {
				horizontal = (-distX > distY);
			}
			else {
				horizontal = (-distX > -distY);
			}
		}

		//Selects the next cell to visit
		int newIndex = 0;
		if (horizontal) {
			//The horizontal axis is explored first
			if (distX > 0) { //On the left -> to the right
				if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
					newIndex = index + 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else { //On the right -> to the left
				if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
					newIndex = index - 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			//Vertical axis
			if (distY > 0) { //Too low -> to the top
				if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
					newIndex = index + currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
				if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
					newIndex = index - currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else { //'Too high -> to the bottom
				if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
					newIndex = index - currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
				if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
					newIndex = index + currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			//Last possible direction
			if (distX > 0) {
				if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
					newIndex = index - 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else {
				if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
					newIndex = index + 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
		}
		else {
			//The vertical axis is explored first		
			if (distY > 0) { //Too low -> to the top
				if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
					newIndex = index + currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else { //Too high -> to the bottom
				if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
					newIndex = index - currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			//Horizontal axis
			if (distX > 0) { //On the left -> to the right
				if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
					newIndex = index + 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
				if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
					newIndex = index - 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else { //On the right -> to the left
				if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
					newIndex = index - 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
				if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
					newIndex = index + 1;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			//Last possible direction
			if (distY > 0) {
				if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
					newIndex = index - currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
			else {
				if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
					newIndex = index + currentMatrix->sizeX;
					if (workingArray[newIndex + 1] == 0) {
						currentMatrix->waypointX.push_back(x);
						currentMatrix->waypointY.push_back(y);
						currentMatrix->currentState.push(newIndex);
						continue;
					}
				}
			}
		}

		//Removes the current cell from the solution array
		currentMatrix->currentState.pop();

		if (currentMatrix->waypointX.size() + currentMatrix->waypointX.size() >= 2) {
			currentMatrix->waypointX.pop_back();
			currentMatrix->waypointY.pop_back();
		}
		result = false;
	}
	return result;
	//Based on Informatix's SDA algorithm: www.b4x.com/android/forum/threads/optimization-with-b4a.57913
}

bool privateRecursiveFLMAlgoSDA(int index)
{
	//Marks as visited
	int *workingArray = (int*)currentMatrix->childAddress;
	workingArray[index + 1] = 1;

	//Computes the horizontal and vertical distances
	int y = (int)(index * currentMatrix->invSizeX);
	int x = index - (y * currentMatrix->sizeX);
	int distX = (currentMatrix->destX) - x;
	int distY = (currentMatrix->destY) - y;

	//Until proven otherwise the current cell is part of the solution	
	currentMatrix->waypointX.push_back(x);
	currentMatrix->waypointY.push_back(y);

	//If the goal is reached...
	if (index == currentMatrix->destIndex) return true;

	//Increment Stack Frame counter
	currentMatrix->stackFrames++;
	if (currentMatrix->stackLimit > 0 && currentMatrix->stackFrames >= currentMatrix->stackLimit) return false;

	//Selects the axis to follow
	bool horizontal = false;
	if (distX > 0) {
		if (distY > 0) {
			horizontal = (distX > distY);
		}
		else {
			horizontal = (distX > -distY);
		}
	}
	else {
		if (distY > 0) {
			horizontal = (-distX > distY);
		}
		else {
			horizontal = (-distX > -distY);
		}
	}

	//Selects the next cell to visit
	int newIndex = 0;
	if (horizontal) {
		//The horizontal axis is explored first
		if (distX > 0) { //On the left -> to the right
			if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
				newIndex = index + 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else { //On the right -> to the left
			if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
				newIndex = index - 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		//Vertical axis
		if (distY > 0) { //Too low -> to the top
			if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
				newIndex = index + currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
			if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
				newIndex = index - currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else { //'Too high -> to the bottom
			if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
				newIndex = index - currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
			if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
				newIndex = index + currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		//Last possible direction
		if (distX > 0) {
			if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
				newIndex = index - 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else {
			if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
				newIndex = index + 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
	}
	else {
		//The vertical axis is explored first		
		if (distY > 0) { //Too low -> to the top
			if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
				newIndex = index + currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else { //Too high -> to the bottom
			if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
				newIndex = index - currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		//Horizontal axis
		if (distX > 0) { //On the left -> to the right
			if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
				newIndex = index + 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
			if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
				newIndex = index - 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else { //On the right -> to the left
			if (x != 0 && currentMatrix->value[x - 1][y] == 0) {
				newIndex = index - 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
			if (x != (currentMatrix->sizeX - 1) && currentMatrix->value[x + 1][y] == 0) {
				newIndex = index + 1;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		//Last possible direction
		if (distY > 0) {
			if (y != 0 && currentMatrix->value[x][y - 1] == 0) {
				newIndex = index - currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
		else {
			if (y != (currentMatrix->sizeY - 1) && currentMatrix->value[x][y + 1] == 0) {
				newIndex = index + currentMatrix->sizeX;
				if (workingArray[newIndex + 1] == 0 && privateRecursiveFLMAlgoSDA(newIndex)) {
					return true;
				}
			}
		}
	}

	//Removes the current cell from the solution array
	currentMatrix->waypointX.pop_back();
	currentMatrix->waypointY.pop_back();
	return false;

	//Based on Informatix's SDA algorithm: www.b4x.com/android/forum/threads/optimization-with-b4a.57913
}